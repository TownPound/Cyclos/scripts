 //import
import java.util.Date;
import java.text.SimpleDateFormat;

import org.apache.commons.lang.time.DateUtils

import org.cyclos.services.banking.AccountService
import org.cyclos.entities.banking.Account
import org.cyclos.entities.banking.UserAccountType
import org.cyclos.entities.system.CustomFieldPossibleValue
import org.cyclos.entities.users.User
import org.cyclos.entities.users.UserGroup
import org.cyclos.impl.users.UserServiceLocal
import org.cyclos.impl.utils.QueryHelper
import org.cyclos.impl.utils.persistence.DBQuery
import org.cyclos.model.banking.accounts.AccountHistoriesOverviewQuery
import org.cyclos.model.banking.accounts.AccountVO
import org.cyclos.model.banking.accounts.AccountWithStatusVO
import org.cyclos.model.banking.accounttypes.AccountTypeNature
import org.cyclos.model.banking.transferfilters.TransferFilterVO
import org.cyclos.model.users.groups.BasicGroupVO
import org.cyclos.model.users.groups.UserGroupVO
import org.cyclos.model.users.users.UserOrderBy
import org.cyclos.model.users.users.UserQuery
import org.cyclos.model.users.users.UserWithFieldsVO
import org.cyclos.model.utils.DatePeriodDTO
import org.cyclos.model.utils.ModelHelper
import org.cyclos.server.utils.DateHelper
import org.cyclos.services.users.UserService

import org.cyclos.model.users.records.UserRecordQuery;
// import org.cyclos.model.users.records.UserRecordSearchData;
import org.cyclos.services.users.RecordService;
import org.cyclos.services.users.RecordTypeService;

import org.cyclos.entities.banking.Transfer
import org.cyclos.entities.users.RecordCustomField
import org.cyclos.entities.users.SystemRecord
import org.cyclos.entities.users.SystemRecordType
import org.cyclos.entities.users.User
import org.cyclos.entities.users.UserRecord
import org.cyclos.entities.users.UserRecordType
import org.cyclos.impl.system.ScriptHelper
import org.cyclos.impl.users.RecordServiceLocal
import org.cyclos.impl.utils.persistence.EntityManagerHandler
import org.cyclos.model.EntityNotFoundException
import org.cyclos.model.banking.accounts.AccountHistoryEntryVO
import org.cyclos.model.banking.accounts.AccountHistoryOverviewEntryVO
import org.cyclos.model.banking.accounts.AccountHistoryQuery
import org.cyclos.model.banking.accounts.AccountHistoryStatusVO
import org.cyclos.model.banking.accounts.InternalAccountOwner
import org.cyclos.model.banking.transfertypes.TransferTypeVO
import org.cyclos.model.system.fields.CustomFieldVO
import org.cyclos.model.system.fields.CustomFieldValueForSearchDTO
import org.cyclos.model.users.records.RecordDataParams
import org.cyclos.model.users.records.UserRecordDTO
import org.cyclos.model.users.records.UserRecordVO
import org.cyclos.model.users.recordtypes.RecordTypeVO
import org.cyclos.model.users.users.UserLocatorVO
import org.cyclos.model.users.users.UserVO
import org.cyclos.utils.Page
import org.cyclos.utils.ParameterStorage

import org.cyclos.model.CyclosException
import org.bristolpound.cyclos.scripting.library.errorHandling.ErrorHandling;


info.addError( 1, "pageContext " + pageContext);
info.addError( 1, "isCSV " + isCSV);
info.addError( 1, "isPDF " + isPDF);
info.addError( 1, "isPAGE " + isPAGE);
// return info;


dateTo		= formParameters.dateTo;
dateFrom	= formParameters.dateFrom;

			def period = ModelHelper.datePeriod(
				conversionHandler.toDateTime(dateFrom),
				conversionHandler.toDateTime(dateTo));
				
				
			def AHQ = 	new AccountHistoryQuery([
							period:			period,
							//orderBy:		org.cyclos.model.banking.transactions.TransactionOrderBy.DATE_ASC,
							// unlimited:		(pageContext != org.cyclos.model.system.operations.CustomOperationPageContext.PAGE),
							//currentPage:	isPAGE ? currentPage	: 0,
							//pageSize:		isPAGE ? pageSize		: Integer.MAX_VALUE,
						]);
			
			// def ass = new org.cyclos.security.banking.AccountServiceSecurity();
			// def ass = new org.cyclos.services.banking.AccountService();
			// def pt = ass.searchAccountHistory(AHQ);
			// def pt = org.cyclos.services.banking.AccountService.searchAccountHistory(AHQ);
			
			org.cyclos.impl.banking.AccountServiceLocal ass = binding.accountService;
			def pt = ass.searchAccountHistory(AHQ);
return info;

				Page<AccountHistoryEntryVO> pageTransactions = 
					accountService.searchAccountHistory(
						new AccountHistoryQuery([
							period:			period,
							//orderBy:		org.cyclos.model.banking.transactions.TransactionOrderBy.DATE_ASC,
							// unlimited:		(pageContext != org.cyclos.model.system.operations.CustomOperationPageContext.PAGE),
							//currentPage:	isPAGE ? currentPage	: 0,
							//pageSize:		isPAGE ? pageSize		: Integer.MAX_VALUE,
						])
					);

					List 				rows 		= [];
					
					
			for (AccountHistoryEntryVO r : pageTransactions.pageItems)
			{
				
				rows <<[other:				r.relatedAccount.type.nature == AccountTypeNature.SYSTEM ?  'System' : r.relatedAccount.owner.shortDisplay,
						account:			formatter.format(r.relatedAccountName ?: "N/A"),
						type:				r.type.name,
						date:				formatter.format(r.date ?: "N/A"),
						amount:				formatter.format(r.amount ?: "N/A"),
						transNo:			r.transactionNumber,
						description:		formatter.format(r.description ?: r.type.name)
					]
				
			}
			
		// Build the result
		return [
			columns: [
				[header: "Date", property: "date", width: "16%", align: "left"],
				[header: "Member (from/to)", property: "other", width: "15%"],
				// [header: "Account", property: "account", width: "20%"],
				// [header: "Type", property: "type", width: "2.5%", align: "right"],
				[header: "Trans #", property: "transNo", width: "10%", align: "left"],
				[header: "Description", property: "description", align: "left"	],
				[header: "Amount", property: "amount", width: "10%", align: "right"],
			],
			rows: rows,
			totalCount: rows.size()
		]
