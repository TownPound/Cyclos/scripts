/**
 *
 * Error handling library for scripts
 *
 * Version:
 *		0.3, 2016-08-22
 *
 * Script parameters:
 * 		none
 *
 * Included libraries:
 *		- libRecords
 *
 * Authors:
 *		- martin.rueegg@bristolpound.org
 *
 * Known issues:
 *		none so far
 *
***/

package org.bristolpound.cyclos.scripting.library.errorHandling;

import org.bristolpound.cyclos.scripting.library.records.ResultRow;
import org.bristolpound.cyclos.scripting.library.records.ResultList;


class ResultError extends ResultRow
{
	int			number 			= 0;
	String		description		= "empty error";
	int			totalCount		= 1;
	
	ResultError(int errNumber, String errDescription)
	{
		this.number = errNumber;
		this.description = errDescription;
	}
	
	public getColumns()
	{
		return [
			[header: "Error", property: "number", width: "10%", align: "left"],
			[header: "Description", property: "description", width: "90%", align: "left"],
			]
	}

	public getRows()
	{
		return [[number: this.number, description: this.description]]
	}
	
	public getResult()
	{
		return [
			columns: this.getColumns(),
			rows: this.getRows(),
			totalCount: 1
		]
	}
}

class ResultInfoList extends ResultList
{
	ResultInfoList()
	{
		this.addColumn([header: "#", property: "index", width: "5%", align: "right"]);
		this.addColumn([header: "Error", property: "number", width: "10%", align: "left"]);
		this.addColumn([header: "Description", property: "description", width: "85%", align: "left"]);
	}
}

def info = new ResultInfoList();


