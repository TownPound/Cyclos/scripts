//===========================================
//------  wsSMS  ----------------------------
//
// @version: 0.0.1

import org.apache.commons.lang3.StringUtils
import org.cyclos.model.utils.ResponseInfo;


// check version
if (!pathVariables.isSet("ver"))
{
	return wsCreateResponse(404, "missing version in URL '"+path+"'");
}

String apiVersion = StringUtils.removeStart(pathVariables.getString("ver"), "v");

try
{
	Integer.parseInt(apiVersion);
	return wsCreateResponse(404, "Unsupported API version '"+apiVersion+"'");
}
catch(ex)
{
	return wsCreateResponse(404, "Unsupported version string '"+apiVersion+"'");
}

return wsCreateResponse(400, "Unexpectd Error");

//------  wsSMS  ----------------------------
//===========================================
